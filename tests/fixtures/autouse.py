# Copyright (C) 2017 Michal Goral.
# 
# This file is part of Subconvert
# 
# Subconvert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Subconvert is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Subconvert. If not, see <http://www.gnu.org/licenses/>.

import pytest
import os

@pytest.fixture(scope='session', autouse=True)
def qsettings_dir(tmpdir_factory):
    '''Makes sure that user config is untouched by tests.'''
    cfgdir = tmpdir_factory.mktemp('config')
    os.environ['XDG_CONFIG_HOME'] = str(cfgdir)
