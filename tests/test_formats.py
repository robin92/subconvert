"""
Copyright (C) 2016 Michal Goral.

This file is part of Subconvert

Subconvert is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Subconvert is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Subconvert. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import pytest

from subconvert.parsing.Core import SubParser
from subconvert.parsing.Formats import (MicroDVD, SubRip, SubViewer, TMP, MPL2,
                                        find_tag, find_all_tags)

@pytest.fixture
def parser():
    parser = SubParser()
    parser.registerFormat(MicroDVD)
    parser.registerFormat(SubRip)
    parser.registerFormat(SubViewer)
    parser.registerFormat(TMP)
    parser.registerFormat(MPL2)
    return parser


def assert_tag(tag, text, pos):
    assert tag.text == text
    assert tag.start == pos
    assert tag.end == tag.start + len(text) + 1


def _test_sub_impl(parser, subtitle):
    parser.parse(subtitle.file.read())
    fmt = parser.parsedFormat()
    subs = parser.results

    descr = subtitle.description

    assert fmt.NAME == descr['format']
    assert fmt.OPT == descr['opt']

    assert len(descr['subs']) == len(subs)
    for i, check in enumerate(descr['subs']):
        sub = subs[i]
        assert sub.start == check.start
        if check.end:
            assert sub.end == check.end
        assert sub.text == check.text

    if descr.get('meta') is not None:
        for key in descr['meta']:
            subs.meta.get(key) == descr['meta'][key]


def test_correct_subs(parser, subtitle):
    '''OK case: correct subs'''
    _test_sub_impl(parser, subtitle)

def test_incorrect_subs(parser, subtitle_nook):
    '''These subs are technically incorrect, but we expect them to be parsed
    anyway, because the errors are easily recoverable'''
    _test_sub_impl(parser, subtitle_nook)

def test_output(parser, subtitle):
    '''Check whether formats correctly output previously parsed subtitles.'''
    parser.parse(subtitle.file.read())
    fmt = parser.parsedFormat()
    subs = parser.results

    out_fmt = fmt(subtitles=subs)
    orig_lines = subtitle.file.read()

    assert ''.join(out_fmt.output).strip() == \
            os.linesep.join(orig_lines).strip()


def test_find_tags():
    #              1111111111
    #    01234567890123456789
    s = '[tag1]abc[tag2][tag3'
    assert_tag(find_tag(s, '[', ']'), 'tag1', 0)
    assert_tag(find_tag(s, '[', ']', start=1), 'tag2', 9)
    assert_tag(find_tag(s, '[', ']', start=5), 'tag2', 9)
    assert_tag(find_tag(s, '[', ']', start=8), 'tag2', 9)
    assert_tag(find_tag(s, '[', ']', start=9), 'tag2', 9)
    assert find_tag(s, '[', ']', start=10).text is None

    #     000000000011111
    #     012345678901234
    s1 = '[tag1[tag2]]abc'
    assert_tag(find_tag(s1, '[', ']'), 'tag1[tag2]', 0)
    assert_tag(find_tag(s1, '[', ']', start=1), 'tag2', 5)
    assert_tag(find_tag(s1, '[', ']', start=5), 'tag2', 5)
    assert find_tag(s1, '[', ']', start=6).text is None

    #     00000000001
    #     01234567890
    s2 = '[notag[tag]'
    assert_tag(find_tag(s2, '[', ']'), 'tag', 6)

    s3 = '012[]567'
    assert_tag(find_tag(s3, '[', ']'), '', 3)

    #     000000000011111111112
    #     012345678901234567890
    s4 = '[notag[tag1]abc[tag2]'
    assert_tag(find_tag(s4, '[', ']'), 'tag1', 6)

    #     0000000000111111111122
    #     0123456789012345678901
    s5 = '[notag[tag1]abc[tag2]]'
    assert_tag(find_tag(s5, '[', ']'), 'notag[tag1]abc[tag2]', 0)


def test_find_all_tags():
    #              111111111122222222223333333333444444444455
    #    0123456789012345678901234567890123456789012345678901
    s = '[tag1]aaa[tag2]bbb[tag3][tag4][nottag[tag5]aaa[tag6]'
    tags = find_all_tags(s, '[', ']')

    assert len(tags) == 6
    assert_tag(tags[0], 'tag1', 0)
    assert_tag(tags[1], 'tag2', 9)
    assert_tag(tags[2], 'tag3', 18)
    assert_tag(tags[3], 'tag4', 24)
    assert_tag(tags[4], 'tag5', 37)
    assert_tag(tags[5], 'tag6', 46)
