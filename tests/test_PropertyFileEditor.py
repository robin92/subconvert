# Copyright (C) 2017 Michal Goral.
# 
# This file is part of Subconvert
# 
# Subconvert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Subconvert is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Subconvert. If not, see <http://www.gnu.org/licenses/>.

import pytest

from subconvert.gui.PropertyFileEditor import PropertyFileEditor

@pytest.fixture
def editor(qtbot, subparser):
    w = PropertyFileEditor(subparser.formats)
    qtbot.addWidget(w)
    w.show()
    return w


def test_show(editor):
    assert editor.isVisible()
