# Copyright (C) 2016 Michal Goral.
# 
# This file is part of Subconvert
# 
# Subconvert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Subconvert is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Subconvert. If not, see <http://www.gnu.org/licenses/>.

import os

from subconvert.apprunner import main

def test_outputs_same(subtitle, args, capsys, tmpdir):
    '''Checks what happens with and without -o/--output-file'''
    # No -o: subconvert prints to stdout
    cmd_stdout = ['-c', '-t', subtitle.description['opt'], subtitle.file.path]
    args(cmd_stdout)
    assert 0 == main()

    out, _ = capsys.readouterr()

    # -o present: subconvert writes to that file
    fp = os.path.join(str(tmpdir.mkdir('sub')), 'subtitle.txt')
    cmd_file = ['-c', '-t', subtitle.description['opt'], '-o', fp,
                subtitle.file.path]
    args(cmd_file)
    assert 0 == main()

    assert os.path.isfile(fp)
    with open(fp) as f_:
        fout = ''.join(f_.readlines())

    # XXX: for some formats subconvert adds a newline at the end. They don't
    # matter, but removing this would mean that we'd have to change a lot of
    # logic. So instead, we'll just strip everything at the end.
    assert os.linesep.join(subtitle.file.read()).rstrip() == out.rstrip()
    assert os.linesep.join(subtitle.file.read()).rstrip() == fout.rstrip()
